import importlib.metadata
import os

# colors for terminal output
red = '\033[91m'
green = '\033[92m'
blue = '\033[94m'
end = '\033[0m'

# MARK: pillow check
# check for the presence of `pillow` module
print(blue+'[1] checking for required module (PIL)'+end)
try:
    importlib.metadata.distribution('pillow')
except importlib.metadata.PackageNotFoundError:
    print(red+'PIL is not installed,trying to set it up automatically'+end)
    if os.name == 'nt' :
        print('installing pip the windows way!')
        os.system('py -m ensurepip --upgrade')
    else : 
        print('installing pip the unix way')
        os.system('python -m ensurepip --upgrade')
    os.system('python3 -m pip install --upgrade Pillow')
else:
    print(green+'\tpillow is already installed, proceding'+end)

try:
    from PIL import Image
except:
    print(red+'INSTALLATION FAILED, please install the "pillow" module following instructions here: https://pillow.readthedocs.io/en/stable/installation/basic-installation.html'+end)
    quit()


# MARK: file check
print(blue+'[2] opening files'+end)
map_regions = os.path.dirname(__file__)+'/map_regions.tga'
if os.path.exists(map_regions) : print('\tfound map_regions.tga')
else : 
    print(red+'\terror : could not find the file map_regions.tga in the same directory as the script'+end)
    quit()

# MARK: processing
with Image.open(map_regions) as img:
    with open(r"settlements_coordinates.txt", "w") as result:
        x, y = img.size
        result.write(f"MAP SIZE : x {x}, y {y}\n\n") 
        print(green+f'\tmap size : x {x}, y {y}'+end)
        print(blue+'[3] selecting the area to export'+end)
        xMin = input('\tinput minimum x value (default : 0) :') or 0
        xMin = int(xMin)
        xMax = input(f'\tinput maximum x value (default : {x}) :') or x
        xMax = int(xMax)
        yMin = input('\tinput minimum y value (default : 0) :') or 0
        yMin = int(yMin)
        yMax = input(f'\tinput minimum y value (default : {y}) :') or y
        yMax = int(yMax)
        print(green+f'\tselected area : x {xMin} to {xMax}, y {yMin} to {yMax}'+end)
        # get every pixel, test for black and output coortinates to file
        print(blue+"[3] Exporting settlements coordinates"+end)
        yMin = y-1-yMin
        yMax = y-1-yMax
        for i in range(xMin,xMax):
            for j in range(yMax, yMin):
                color = img.getpixel((i,j))
                # test like this to avoid error with alpha channel
                if (color[0], color[1], color[2]) == (0, 0, 0):
                    result.write(f"coordinates found: x {i}, y {y-1-j}\n")
                    print(f"\tcoordinates found: x {i}, y {y-1-j}")
        result.write("\nFinished processing map")
        print(green+"\tFinished processing map"+end)
print(blue+'SCRIPT FINISHED'+end)